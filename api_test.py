import unittest

from webservice_flask import app


class ApiTest(unittest.TestCase):

    def setUp(self):
        self.deal = """data:{"instrumentName": "Galactia", "cpty": "Lewis", "price": 9964.235074757127, "type": "S", 
    "quantity": 71, "time": "11-Aug-2019 (12:07:06.471252)"} """

        self.app = app.test_client(self)
        app.config['TESTING'] = True

    def test_deal_endpoint(self):
        response = self.app.post('/deal', data=self.deal)
        print(response)
        expected_response_code = '200 OK'
        self.assertEqual(expected_response_code, response.status, msg="Response is not successful")
