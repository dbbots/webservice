import json
import logging
from queue import Queue

from flask import Flask, Response, request
from flask_cors import CORS
import requests

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

app = Flask(__name__)
CORS(app)
deal_queue = Queue()
app.config.from_object('config')

LOGIN_URL = app.config["SECURITY_URL"] + "/auth/login"
USER_STATUS_URL = app.config["SECURITY_URL"] + "/auth/status"
CTPY_URL = app.config["DAO_URL"] + "/ctpy"


@app.route('/login', methods=['POST'])
def auth_login():
    request_body = request.get_json()
    response = login_response(request_body)
    if response["status"] == "success":
        return response, 200
    return auth_fail_response(response["status"])


@app.route('/ctpy/tradecount', methods=['GET'])
def ctpy_trade_count():
    auth_header = request.headers.get('Authorization')
    status = check_auth(auth_header)
    if status == "success":
        return get_ctpy_count()
    return auth_fail_response(status)


@app.route('/dealdata', methods=['POST'])
def receive_deal():
    request_data = request.get_json()
    logging.info(f"new request: {request_data}")
    deal_queue.put(request_data)
    return '', 200


def filter_deal_by_price(deal):
    dict_deal = json.loads(deal)
    new_dict = {
        "instrumentName": dict_deal["instrumentName"],
        "price": dict_deal["price"],
        "time": dict_deal["time"],
        "type": dict_deal["type"]
    }
    return json.dumps(new_dict)


@app.route('/dealStream')
def sse_stream():
    def eventStream():
        while True:
            deal = deal_queue.get()
            deal = filter_deal_by_price(deal)
            logging.info(f"Sending deal: {deal}")
            yield 'data:{}\n\n'.format(deal)

    return Response(eventStream(), mimetype="text/event-stream")


def login_response(request_body):
    json_response = requests.post(url=LOGIN_URL, json=request_body).json()
    return json_response


def check_auth(auth_header):
    json_response = requests.get(url=USER_STATUS_URL, headers={"Authorization": auth_header}).json()
    print(json_response)
    return json_response["status"]


def get_ctpy_count():
    json_response = requests.get(url=CTPY_URL).json()
    print(json_response)
    if len(json_response) == 0:
        return {}, 200
    return json_response, 200


def auth_fail_response(status):
    return status, 404


def bootapp():
    app.run(port=app.config["FLASK_PORT"], threaded=True, host=app.config["FLASK_HOST"])


if __name__ == '__main__':
    bootapp()
