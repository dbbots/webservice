from os import getenv

FLASK_PORT = '8081'
FLASK_HOST = '0.0.0.0'

SECURITY_URL = getenv("SECURITY_URL")
DAO_URL = getenv("DAO_URL")

